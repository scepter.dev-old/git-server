FROM ubuntu:18.04

WORKDIR /app

RUN apt-get update
RUN apt-get install -y curl software-properties-common git
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs

ENV PATH /app/node_modules/.bin:$PATH

COPY ./package*.json ./

RUN npm install --silent

COPY . /app

EXPOSE 7000
EXPOSE 7001

CMD ["node", "index"]
