const Git = new (require("./Git"))();
const express = require('express');
const app = express();

const servicePort = process.env.SERVICEPORT || 7001;

app.get('/:project/:repo/files/:branch/*', (req, res) => {
    const repo = (req.params.project || "") + "/" + (req.params.repo || "") + ".git";
    const branch = req.params.branch || "master";
    const pathToRepo = require("path").resolve("../repos/", repo);
    const path = req.params[0].split("/");
    Git.listPathFiles(pathToRepo, branch, path).then((files) => {
        res.json(files);
    }).catch(error => {
        console.log(error);
        res.json({error: error});
    });
});

app.get('/:project/:repo/branches', (req, res) => {
    const repo = (req.params.project || "") + "/" + (req.params.repo || "") + '.git';
    const pathToRepo = require("path").resolve("../repos/", repo);
    Git.listRepoBranches(pathToRepo).then((branches) => {
        res.json(branches);
    }).catch(error => {
        console.log(error);
        res.json({error: "an error occurred"});
    });
});
app.listen(servicePort, () => console.log(`service running at port ${servicePort}!`));
