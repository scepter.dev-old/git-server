const nodegit = require("nodegit");

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

function listWholeTreeFiles(tree, level, files, parent = null) {
    return new Promise(async (resolve) => {
        await asyncForEach(tree.entries(), async entry => {
            let indent = "";
            for (let i = 0; i < level; i++) {
                indent = indent.concat("    ")
            }
            let object = {name: entry.name()};

            if(parent){
                parent.children.push(object);
            } else {
                files.push(object);
            }

            if (entry.isTree()) {
                object.children = [];
                const tree = await entry.getTree();
                await listWholeTreeFiles(tree, level + 1, files, object);
            }
        }).then(() => {
            console.log(files);
            resolve();
        });
    });
}

async function getPathFiles(commit, treePath) {
    let tree = await commit.getTree();
    let files = tree.entries();
    return new Promise(async (resolve) => {
        await asyncForEach(treePath, async path => {
            const entry = tree.entries().find(entry => entry.name() === path);
            if(entry){
                if (entry.isTree()) {
                    tree = await entry.getTree();
                    files = tree.entries();
                } else {
                    resolve(entry);
                }
            }
        }).then(() => {
            resolve(files);
        });
    });
}

async function listRepoFiles(pathToRepo, branch, files) {
    const repo = await nodegit.Repository.open(pathToRepo);
    const commit = await repo.getBranchCommit(branch);
    const tree = await commit.getTree();

    await listWholeTreeFiles(tree, 0, files);
}

async function listRepoBranches(pathToRepo) {
    const repo = await nodegit.Repository.open(pathToRepo);
    return repo.getReferenceNames(3);
}
module.exports = class Git {
    listPathFiles = async (repoPath, branch, treePath) => {
        const repo = await nodegit.Repository.open(repoPath);
        const commit = await repo.getBranchCommit(branch);
        const treeFiles = await getPathFiles(commit, treePath);
        if(Array.isArray(treeFiles)) {
            const files = [];
            treeFiles.forEach(file => {
                files.push({name: file.name(), isTree: file.isTree()})
            });
            return {type: 'tree', files};
        } else {
            const entry = treeFiles;
            const blob = await entry.getBlob();
            const name = entry.name();
            const file = blob.toString();
            return {type: 'file', name, file};
        }
    };
    listRepoBranches = async (repoPath) => {
        return await listRepoBranches(repoPath);
    };
};
