const path = require('path');
const Server = require('node-git-server');
const serverPort = process.env.SERVERPORT || 7000;
const request = require('request-promise-native');

const projectService = process.env.PROJECT_SERVICE || 'http://localhost:3001';

const repos = new Server(path.resolve(__dirname, '../repos'), {
    autoCreate: true,
    authenticate: ({type, repo, user}, next) => {
        console.log(repo);
        const repoPath = repo.split("/");
        if (repoPath.length !== 2) {
            return next("Invalid path");
        }

        const project = repoPath[0];
        const repository = repoPath[1];

        user((username, password) => {
            const uri = `${projectService}/projects/${project}/repositories/${repository}/credentials/verify`;
            request({url: uri, method: 'POST', body: {username, password}, json: true}).then(() => {
                return next();
            }).catch(() => {
                return next("Invalid credentials");
            });
        });
    }
});

repos.on('push', (push) => {
    console.log(`push ${push.repo}/${push.commit} (${push.branch})`);
    push.accept();
});

repos.on('fetch', (fetch) => {
    console.log(`fetch ${fetch.commit}`);
    fetch.accept();
});

repos.on('pull', (pull) => {
    console.log(`pull ${pull}`);
    pull.accept();
});

repos.listen(serverPort, () => {
    console.log(`server running at port ${serverPort}`);
});
